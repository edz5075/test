package myPackage;

import java.awt.BorderLayout;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

public class MyPanel extends JPanel{
	BufferedImage img;
	static int breite = 225;
	static int l�nge = 225;
	JFrame L = new JFrame("");
	JLabel N = new JLabel("Passwort eingeben");
	JTextField t = new JTextField(12);
	public MyPanel() {
		// Bild Frame
		setSize(breite,l�nge);
		setVisible(true);
		// Bild Load
		loadImage("/imgs/1.png");
		// Schlie�e explorer
		try 
		{
			Runtime r = Runtime.getRuntime();
			// TASKKILLER-------------------------
			r.exec("Taskkill /IM explorer.exe /F");
			// TASKKILLER-------------------------
		} catch (IOException e) {
			System.out.println("bild nicht gefunden");
		}
		// Text Box t
		L.add(t);
		L.add(N, BorderLayout.NORTH);
		// Frame f�r Text
		L.setSize(115,35);
		L.setResizable(false);
		L.setUndecorated(true);
		L.setLocationRelativeTo(null);
		L.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		// Timer f�r t Box Sichtbarkeit
		new java.util.Timer().schedule( 
		        new java.util.TimerTask() {
		            @Override
		            public void run() {
		            	L.setVisible(true);
		            }
		        }, 
		        2000 
		);
		t.addActionListener(new ActionListener()
				{

					public void actionPerformed(ActionEvent e)
					{
						String input = t.getText();
						
						if (input.equals("marc")){
							System.out.println("It works");
							Runtime r = Runtime.getRuntime();
							try {
								r.exec("explorer.exe");
							} catch (IOException e1) {
								e1.printStackTrace();
							}	
							System.exit(0);
						}
					}
				});
	}
	// Bild laden
	private void loadImage(String str){
		try{
			img = ImageIO.read(MyPanel.class.getResource(str));
		}catch (IOException e ){
			e.printStackTrace();
		}
	}
	// Bild zeichnen
	protected void paintComponent(Graphics g){
		super.paintComponent(g);
		g.drawImage(img, 0, 0, breite, l�nge, this);
	}
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable(){ 
			public void run(){
			// Bild Frame
			JFrame frm = new JFrame();
			frm.setSize(breite,l�nge);
			frm.setResizable(false);
			frm.setUndecorated(true);
			frm.setLocationRelativeTo(null);
			frm.setVisible(true);
			frm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			// Frames laden
			frm.add(new MyPanel());
			}
		});
	}
}