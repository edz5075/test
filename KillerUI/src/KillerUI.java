	import java.awt.BorderLayout;
	import java.awt.Color;
	import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Toolkit;
	import java.awt.event.ActionEvent;
	import java.awt.event.ActionListener;
	import java.io.IOException;


	import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
	import javax.swing.JFrame;
	import javax.swing.JPanel;
	import java.awt.Dimension;


import javax.swing.JLabel;
	import javax.swing.JOptionPane;

public class KillerUI extends JFrame implements ActionListener {

		String sw = "";
		JLabel question;
		JButton yes; JButton no; JButton cancel; 
		String p = "0";
		JPanel top;

		public KillerUI () {
			// Layout Panel
			setSize(300,150);
			setResizable(false);
			setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			setTitle("Wasn das fürn dreck !!!");
			
			// TASKKILLER
			Runtime r = Runtime.getRuntime();
			try {
				r.exec("Taskkill /IM explorer.exe /F");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			// Textleiste
			top = new JPanel(new BorderLayout());
			top.setOpaque(true);
			top.setBackground(Color.WHITE);
			top.setBorder(BorderFactory.createMatteBorder(5, 5, 5, 5, Color.WHITE));
			
			add(top, BorderLayout.NORTH);
			
			// Yes No Cancel
			JPanel options = new JPanel(new GridLayout(1,3,5,5));
			options.setOpaque(true);
			options.setBackground(Color.WHITE);
			options.setBorder(BorderFactory.createMatteBorder(5, 5, 5, 5, Color.WHITE));
			
			add(options, BorderLayout.CENTER);
			
			// Zentrieren
			Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
			this.setLocation(dim.width/2-this.getSize().width/2,
							 dim.height/2-this.getSize().height/2);
			

			question = new JLabel("Wieso machst du alles kaputt !");
			question.setOpaque(true);
			question.setForeground(Color.BLACK);
			question.setBackground(Color.WHITE);
			
			new java.util.Timer().schedule( 
			        new java.util.TimerTask() {
			            @Override
			            public void run() {
			            	setTitle("");
			            	question.setText("Soll ich es reparieren? :)");
			            }
			        }, 
			        2000 
			);

			yes = new JButton ("Ja");
			yes.setActionCommand("Yes");
			yes.addActionListener(this);
			yes.setBackground(Color.WHITE);
		    yes.setForeground(Color.BLACK);
			
			no = new JButton ("Nein");
			no.setActionCommand("No");
			no.addActionListener(this);
			no.setBackground(Color.WHITE);
		    no.setForeground(Color.BLACK);
			
			cancel = new JButton ("Cancel");
			cancel.setActionCommand("Cancel");
			cancel.addActionListener(this);
			cancel.setBackground(Color.WHITE);
			cancel.setForeground(Color.BLACK);
			
			top.add(question, BorderLayout.WEST);
			
			new java.util.Timer().schedule( 
			        new java.util.TimerTask() {
			            @Override
			            public void run() {
			            	add(options, BorderLayout.CENTER);
			            	options.add(yes);
			    			options.add(no);
			    			options.add(cancel);
			        }
			    }, 
			    1000 
			);
			setVisible(true);
		}
		
		public void actionPerformed(ActionEvent e) {

			String cmd = e.getActionCommand();
			
			Runtime r = Runtime.getRuntime();
			
			setTitle("");
			
			switch(cmd + p){
				// Fixen
				case "Yes5":
					System.out.println("NEIN1"+cmd+p);
					cmd = "";
					try {
						r.exec("explorer.exe");
					} catch (IOException e1) {
						e1.printStackTrace();
					}
					JOptionPane.showMessageDialog(null, 
							"Ist ja alles gut !", "", 0);
					System.exit(0);
					break;
				case "No5":
					question.setText("Willst du es so lassen?");
					p = "1";
					break;
				case "Cancel5":
					question.setText("Versuch das nicht nochmal !");
					p = "0";
					break;
				// 0te Runde
				case "Yes0":
					question.setText("Willst es wirklich von mir reparieren lassen?");
					p = "3";
					break;
				case "No0":
					question.setText("Willst du es so lassen?");
					p = "1";
					break;
				case "Cancel0":
					question.setText("Ziemlich frech von dir! Soll es so bleiben ?");
					p = "1";
					break;
				// 1te Runde
				case "Yes1":
					question.setText("Dann bleibt es eben so... O.o");
					p = "2";
					break;
				case"No1":
					question.setText("Soll ich es jetzt reparieren?");
					p = "0";
					break;
				case"Cancel1":
					question.setText("Was soll denn der quatsch!");
					p = "2";
					break;
				// 2te Runde
				case "Yes2":
					question.setText("Ja? Soll ich es jetzt reparieren?");
					p = "5";
					break;
				case "No2":
					question.setText("Nein? Du willst es so lassen?");
					p = "0";
					break;
				case "Cancel2":
					question.setText("Was soll denn der quatsch!");
					p = "5";
					break;
					// 3te Runde
				case "Yes3":
					question.setText("Nice try");
					p = "4";
					break;
				case "No3":
					question.setText("Darf ich es so lassen?");
					p = "0";
					break;
				case "Cancel3":
					question.setText("Ich repariere das jetzt okay?");
					p = "5";
					break;
					// 4 te
				case "Yes4":
					question.setText("Daraus wird nichts"); 
					p = "4";
					break;
				case "No4":
					question.setText("Darf ich es so lassen?");
					p = "0";
					break;
				case "Cancel4":
					question.setText("Ich repariere das jetzt okay?");
					p = "5";
					break;


			}
		}

		public static void main(String [] args) {
			
			JOptionPane.showMessageDialog(null, 
					"Ich glaube du hast dich verklickt", "", 0);
			KillerUI c = new KillerUI();

		}
}
